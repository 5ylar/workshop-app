// import React, { Component } from 'react';

// class App extends Component {
//   render() {
//     return (
//       <BrowserRouter>
//         <Route exact path='/' component={component}/>

//       </BrowserRouter>
//     );
//   }
// }
// export default App;


import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Login from './Components/Containers/Login';
import Dashboard from './Components/Containers/Dashboard';
import Layouts from './Components/Views/Layouts';
import Member from './Components/Containers/Member';
import Private from './Components/Containers/Private';

export default () => {
  return (
    <div>
      <BrowserRouter>
        <div>
          <Route exact path='/' component={Private(Layouts(Dashboard))} />
          <Route exact path='/members' component={Private(Layouts(Member))} />
          <Route path='/login' component={Login} />
        </div>
      </BrowserRouter>
    </div>
  )
}





