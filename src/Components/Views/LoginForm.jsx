import React, { Component } from 'react'
import Axios from '../../../node_modules/axios';
import {withRouter} from 'react-router-dom'
class LoginForm extends Component {
  state = {
    username:'admin',
    password:'12345678'
  }
  onClickLogin = () => {
    let {username,password} = this.state
    let data = {
      username,
      password
    }
    Axios.post('http://localhost:5555/',data).then( ({data:{token}}) => {
      localStorage.setItem('token',token)
      this.props.history.push('/')
    }).catch( err => {})
  }
  onChange = ({target:{name,value}}) => {
    this.setState({
        [name]:value
    })
  }
  render() {
    let {username,password} = this.state
    return (
      <div className="center">
        <h1>LOGIN</h1>
        <input name="username" type="text" value={username} onChange={this.onChange}/> <br />
        <input name="password" type="password" value={password} onChange={this.onChange}/> <br />
        <button onClick={this.onClickLogin}> LOGIN </button>
      </div>
    )
  }
}

export default withRouter(LoginForm)